#!/bin/sh

# change directory to home
cd home

# create sample directory
mkdir sample

# change directory to sample
cd sample

# create sample.txt file
touch sample.txt

# adds this text to the file
echo "Hi! This is just a sample text file created using shell script" > sample.txt

# show the txt file
cat sample.txt

echo `grep -o "t" sample.txt | wc -l` >> sample.txt

# change user permission
chmod u=rwx sample.txt

# append content to sample.txt
echo "Hi! This is just another sample text added to file." >> sample.txt

# change group permission
chmod g=r sample.txt

# change all user permissions
chmod o= sample.txt

# creates sample2.txt having same content as sample.txt
cat sample.txt > sample2.txt

# loop to add 1000 lines in sample.txt
i=0
while [ $i -ne 1000 ]
do
	i=$(($i+1))
	echo "This is line $i" >> sample.txt
done

# print first 50 lines of sample.txt
head -50 sample.txt

# print last 50 lines of sample.txt
tail -50 sample.txt

# create 5 files
touch prog1.txt prog2.txt program.txt code.txt info.txt

# list files which has "prog" in its name
ls | grep "prog"

# create alias for above program
alias list_prog="ls | grep "prog""

